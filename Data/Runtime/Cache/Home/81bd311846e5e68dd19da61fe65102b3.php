<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo ($cms["page_title"]); ?></title>
    <meta content="<?php echo ($cms["page_keywords"]); ?>" name="keywords" />
    <meta content="<?php echo ($cms["page_description"]); ?>" name="description" />
    <link rel="stylesheet" type="text/css" href="/dev/Templates/<?php echo (C("DEFAULT_THEME")); ?>/css/css1.css" />
    <script src="/dev/Data/Public/org/Jquery/jquery-1.8.2.min.js" type="text/javascript"></script>
    			<script src="/dev/Data/Public/org/Validate/jquery.validate.js" type="text/javascript"></script>
			<script src="/dev/Data/Public/org/Validate/jquery.validate.unobtrusive.js" type="text/javascript"></script>
</head>
<body >
    <!--顶部-->
    <div id="doc">
        <div id="hd">
            <div class="clearfix pagetitle">
                <h1 class="sitename">
                    <a href="/dev/" title="<?php echo (C("cfg_name")); ?>">
                        <img  class="ifixpng" src="/dev/<?php echo (C("cfg_logo")); ?>"  alt="<?php echo (C("cfg_name")); ?>" />
                    </a>
                </h1>
                <div class="language">
                    <div class="language-inner"></div>
                    <form class="search clearfix" action="<?php echo U('/products_l_2');?>">
                        <input id="ipt-keywords" name="keywords" class="enter" type="text" value="" placeholder='请输入关键字...'/>
                        <button class="btn" type="submit"></button>
                    </form>
                </div>
                <div class="login">
                    <?php if(isset($_SESSION['uid']) && isset($_SESSION['username'])): echo (session('username')); ?>，欢迎回来！<a href="<?php echo U('Member/User/index');?>">会员中心</a> | <a href="<?php echo U('Member/Login/out');?>">退出</a>
                    <?php else: ?>
                        <a href="<?php echo U('Member/Login/index');?>">登录</a> | <a href="<?php echo U('Member/Reg/index');?>">注册</a><?php endif; ?>
                </div>
            </div>
            <div class="clearfix sitenav">
                <div class="clearfix menu-main">
                    <ul id="menuSitenav" class="clearfix">
                        <li class="first-item <?php if(MODULE_NAME == "Index"): ?>open<?php endif; ?>">
                            <a href="/dev/" class="home">
                                <span>首页</span>
                            </a>
                        </li>
                        			<?php
 switch ('top') { case 'top': $_nav = D('Category','Service')->get_nav(0); break; case 'son': $_nav = D('Category','Service')->get_nav(0); break; } $k=0; foreach($_nav as $field): if($k>6-1) break; ?>
				<li <?php if($field["cur"]): ?>class='open'<?php endif; ?> >
                            <a href="<?php echo ($field["url"]); ?>" target="_self">
                                <span><?php echo ($field["cname"]); ?></span>
                            </a>
                        </li>
			<?php
 $k++; endforeach;?>

                    </ul>
                </div>
            </div>
        </div>
    </div>

<!--banner-->
<div id="banner-block">
	<div class="banner-block-width">
		<div id="sys-banner">
			<div id="banner-main" class="banner banner-main">
				<div class="banner-inner">
								<?php
 $_lists = D('Ad','Service')->show_ad(1,0); if($_lists): foreach($_lists as $k=>$field): ?>	
					<a href="<?php echo ($field["url"]); ?>" target="_self">
						<img src="<?php echo ($field["pic"]); ?>" width="100%" alt="" />
					</a>
			<?php  endforeach; endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<!--中间区块-->
<div id="content-block">
	<div class="content-block-width">
		<div id="bd">
			<div id="homepg" class="bd-inner">
				<!--检查这里是否为一样-->
				<div class="clearfix layout-home three-cols">
					<div class="main-layout clearfix"></div>
					<div class="col-main">
						<div class="main-wrap">
							<div class="block first-block block-diy list-410" id="block-diy-109067" rel="109067">
								<div class="block-head">
									<div class="head-inner">
										<h2 class="title">公司介绍</h2>

									</div>
								</div>
								<div class="block-content clearfix">
									<div class="content-text">
													<?php
 $_lists = D('Article','Service')->get_flag("18","",0,""); if($_lists): foreach($_lists as $k=>$field): ?>	
					<?php echo ($field["body"]); ?>
			<?php  endforeach; endif; ?>
									</div>
								</div>
								<div class="block-foot">
									<div>
										<div></div>
									</div>
								</div>
							</div>
							<div class="block last-block block-products list-410" id="block-products-109066" rel="109066">
								<div class="block-head">
									<div class="head-inner">
										<h2 class="title">最新产品</h2>
										<div class="links">
														<?php
 $categoryModel = D('Category','Service'); $field = $categoryModel->get_one(2); if($field): ?>	
					<a class="more" href="<?php echo ($field["url"]); ?>">更多</a>
			<?php  endif; ?>
										</div>
									</div>
								</div>
								<div class="block-content clearfix">
									<ul class="list-gallery clearfix">

													<?php
 $_lists = D('Article','Service')->get_flag("2","c",3,""); if($_lists): foreach($_lists as $k=>$field): ?>	
					<li>
											<div class="thumb">
												<a href="<?php echo ($field["url"]); ?>" >
													<img src="<?php echo ($field["pic"]); ?>" alt="<?php echo ($field["article_title"]); ?>" width="100%" />
												</a>
											</div>
											<div class="des">
												<p class="default">
													<a href="<?php echo ($field["url"]); ?>" ><?php echo (cms_substr($field["article_title"],20)); ?></a>
												</p>
											</div>
										</li>
			<?php  endforeach; endif; ?>
									</ul>
								</div>
								<div class="block-foot">
									<div>
										<div></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sub">
						<div class="block first-block block-products  list-270" id="block-products-109065" rel="109065">
							<div class="block-head">
								<div class="head-inner">
									<h2 class="title">产品分类</h2>
									<div class="links">
													<?php
 $categoryModel = D('Category','Service'); $field = $categoryModel->get_one(2); if($field): ?>	
					<a class="more" href="<?php echo ($field["url"]); ?>">更多</a>
			<?php  endif; ?>
									</div>
								</div>
							</div>
							<div class="block-content clearfix">
								<div class="item-list">
									<ul class="clearfix">

													<?php
 switch ('son') { case 'top': $_nav = D('Category','Service')->get_nav(0); break; case 'son': $_nav = D('Category','Service')->get_nav(2); break; } $k=0; foreach($_nav as $field): if($k>20-1) break; ?>
				<li>
											<a title="<?php echo ($field["cname"]); ?>" href="<?php echo ($field["url"]); ?>"><?php echo ($field["cname"]); ?></a>
										</li>
			<?php
 $k++; endforeach;?>
										
									</ul>
								</div>
							</div>
							<div class="block-foot">
								<div>
									<div>-</div>
								</div>
							</div>
						</div>
						<div class="block last-block block-diy list-270" id="block-diy-109068" rel="109068">
							<div class="block-head">
								<div class="head-inner">
									<h2 class="title">联系我们</h2>

								</div>
							</div>
							<div class="block-content clearfix">
								<div class="content-text">
									<span style="white-space:nowrap;">地址：<?php echo (C("cfg_address")); ?></span>
									<br />
									<span style="white-space:nowrap;">电话：<?php echo (C("cfg_tel")); ?></span>
									<br />
									<span style="white-space:nowrap;">邮箱：<?php echo (C("cfg_email")); ?></span>
									<br />
									
								</div>
							</div>
							<div class="block-foot">
								<div>
									<div></div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-extra">
						<div class="block first-block block-articles  list-250" id="block-articles-109059" rel="109059">
							<div class="block-head">
								<div class="head-inner">
									<h2 class="title">最新文章</h2>
									<div class="links">
														<?php
 $categoryModel = D('Category','Service'); $field = $categoryModel->get_one(3); if($field): ?>	
					<a class="more" href="<?php echo ($field["url"]); ?>">更多</a>
			<?php  endif; ?>
									</div>
								</div>
							</div>
							<div class="block-content clearfix">
								<div class="item-list">
									<ul class="clearfix">
													<?php
 $_lists = D('Article','Service')->get_flag("3","",5,""); if($_lists): foreach($_lists as $k=>$field): ?>	
					<li>
											<a title="<?php echo ($field["article_title"]); ?>" href="<?php echo ($field["url"]); ?>"><?php echo (cms_substr($field["article_title"],20)); ?></a>
										</li>
			<?php  endforeach; endif; ?>
										
									</ul>
								</div>
							</div>
							<div class="block-foot">
								<div>
									<div>-</div>
								</div>
							</div>
						</div>
						<div class="block last-block block-articles  list-250" id="block-articles-109069" rel="109069">
							<div class="block-head">
								<div class="head-inner">
									<h2 class="title">推荐文章</h2>
									<div class="links">
													<?php
 $categoryModel = D('Category','Service'); $field = $categoryModel->get_one(3); if($field): ?>	
					<a class="more" href="<?php echo ($field["url"]); ?>">更多</a>
			<?php  endif; ?>
									</div>
								</div>
							</div>
							<div class="block-content clearfix">
								<div class="item-list">
									<ul class="clearfix">
													<?php
 $_lists = D('Article','Service')->get_flag("3","c",5,""); if($_lists): foreach($_lists as $k=>$field): ?>	
					<li>
											<a title="<?php echo ($field["article_title"]); ?>" href="<?php echo ($field["url"]); ?>"><?php echo (cms_substr($field["article_title"],20)); ?></a>
										</li>
			<?php  endforeach; endif; ?>
										
									</ul>
								</div>
							</div>
							<div class="block-foot">
								<div>
									<div>-</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</div>
			<!--底部-->
			<div id="ft">
				<div class="ft-inner">
					<div class="ft-menu"  id="ft-menu">
						<?php echo (nl2br(C("cfg_copyright"))); ?>
						<?php echo (C("cfg_count")); ?>
						<a href="http://www.miibeian.gov.cn/"><?php echo (C("cfg_icp")); ?></a></p>
					</div>
					
					<div>
						<?php echo hook('link','');?>
					</div>
					<div>
						<?php echo hook('thirdlogin','');?>
					</div>

				</div>
			</div>

				
		</div>
			



	</div>

</body>
</html>