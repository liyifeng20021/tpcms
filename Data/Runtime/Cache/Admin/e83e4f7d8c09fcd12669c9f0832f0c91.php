<?php if (!defined('THINK_PATH')) exit();?><!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title>系统后台 - Tpcms内容管理系统 - by Tpcms</title>
<?php if(CONTROLLER_NAME == "Login"): ?><link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_login.css"  />
<link rel="stylesheet" type="text/css" href="/dev/Data/Public/admin/css/admin_default_color.css" />
<?php else: ?>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" /><?php endif; ?>

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/Index'; //当前控制器)
ACTION='/dev/index.php/Admin/Index/copyright';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="/dev/Data/Public/org/jquery.js"></script>
</head>
<body>
<div class="wrap">
  <div id="home_toptip"></div>
  <h2 class="h_a">系统信息</h2>
  <div class="home_info">
    <ul>
      <?php $i=1; ?>
    	<?php if(is_array($info)): foreach($info as $key=>$value): ?><li <?php if($i%2 == 0): ?>style='background:#fff;font-weight:800'<?php endif; ?>>
	     		<em><?php echo ($key); ?></em>
	     		<span ><?php echo ($value); ?></span>
	     	</li>
       <?php $i++; endforeach; endif; ?>
     </ul>
  </div>
  <h2 class="h_a">开发团队</h2>
  <div class="home_info" id="home_devteam">
    <ul>
      <li>
        <em>版权所有</em> 
        <span><a href="http://www.djie.net" target="_blank">http://www.djie.net/</a></span> 
      </li>
      <li style='background:#fff;font-weight:800'>
        <em>负责人</em> 
        <span>点击未来</span> 
      </li>
     
    </ul>
  </div>
</div>
<script type="text/javascript" src="/dev/Data/Public/admin/js/mod.common.js"></script>
<script type="text/javascript" src="/dev/Data/Public/org/artDialog/artDialog.js"></script>
</body>
</html>