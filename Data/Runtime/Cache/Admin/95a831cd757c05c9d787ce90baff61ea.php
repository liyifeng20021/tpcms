<?php if (!defined('THINK_PATH')) exit();?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<title><?php echo (C("cfg_name")); ?> - 提示信息</title>
<link href="/dev/Data/Public/admin/css/admin_style.css" rel="stylesheet" />
<link href="/dev/Data/Public/org/artDialog/skins/default.css" rel="stylesheet" />

<script type='text/javascript'>
MODULE='/dev/index.php/Admin'; //当前模块
CONTROLLER='/dev/index.php/Admin/Ucenter'; //当前控制器)
ACTION='/dev/index.php/Admin/Ucenter/index';//当前方法(方法)
ROOT='/dev'; //当前项目根路径
PUBLIC= '/dev/Data/Public/admin';//当前定义的Public目录
</script>
<script src="/dev/Data/Public/org/wind.js"></script>
<script src="_/dev/Data/Public/org/jquery.js"></script>
</head>
<body>
<div class="wrap">
  <div id="error_tips">
    <h2>操作失败</h2>
    <div class="error_cont">
      <ul>
        <li><?php echo ($message); ?></li>
      </ul>
      <div class="error_return"><a href="" class="btn">请联系管理员</a></div>
    </div>
  </div>
</div>
<script src="/dev/Data/Public/admin/mod.common.js"></script>

</body>
</html>