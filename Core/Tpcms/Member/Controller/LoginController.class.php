<?php
/**[登录]
 * @Author: 976123967@qq.com
 * @Date:   2015-05-11 13:50:54
 * @Last Modified by:   Administrator
 * @Last Modified time: 2015-08-08 18:03:04
 */
namespace Member\Controller;
use Common\Controller\CommonController;
class LoginController extends CommonController{
	public function index()
	{
		$db = D('User','Logic');

		if(IS_POST)
		{
			$username = I('post.username');
			$password = I('post.password');

			//ucenter登录
			if(C('cfg_ucenter'))
			{
				include_once './Data/Config/uc.inc.php';
				include_once './uc_client/client.php';
				list($uid, $username, $password, $email) = uc_user_login(I('post.username') , I('post.password'));
				setcookie('tpcms_auth', '',-1,'/');
				/*大于 0:返回用户 ID，表示用户登录成功
                -1:用户不存在，或者被删除
                -2:密码错
                -3:安全提问错*/
				switch ($uid)
				{
					case -1: //用户不存在
						//删除tpcms中user表用户
						$uid = $db->where(array('username'=>I('post.username')))->getField('uid');
						if($uid)
						{
							$db->delete($uid);
							M('user_baseinfo')->where(array('user_uid'=>$uid))->delete();
						}
						$this->error('用户名或密码错误');
						break;
					case -2: //密码错误
						$this->error('用户名或密码错误');
						break;
					case -3:
						$this->error('安全提问错误');
						break;
				}


				//查找是否存在自己的用户表中
				// p($_POST);
				$password = md5($password);
				$user =$db->where(array('username'=>$username))->find();
				//用户不存在
				if(!$user)
				{

					$username = I('post.username');
					$nickname = explode('@',$username);
					$db->add(array(
							'uid'=>$uid,
							'username'=>$username,
							'nickname'=>$nickname[0],
							'password'=>$password,
							'email'=>$email,
							'addtime'=>time(),
							'role'=>2,
							'times'=>0,
							'login_time'=>time(),
							'login_ip'=>get_client_ip(),
					));
					$user['uid']= $uid;
					$user['uername']= $username;
					$user['email'] = $email;
					//同步登录
				}
				elseif($user['password']!=$password)
				{
					$this->error('该用户不能登录，请联系网站管理员');
				}
			}
			else
			{

				$where['username'] = $username;
				$user = $db->where($where)->find();
				if(!$user)
					$this->error('用户名或密码错误');
				if($user['password']!=md5($password))
					$this->error('用户名或密码错误');
			}


			session('uid',$user['uid']);
			session('username',$user['username']);
			session('email',$user['email']);
			$db->save(array('uid'=>$user['uid'],'login_times'=>$user['logign_times']+1,'login_ip'=>get_client_ip()));
			


			//同步登录
			if(C('cfg_ucenter'))
			{
				//设置cookie
				setcookie('tpcms_auth', uc_authcode($uid."\t".$username, 'ENCODE'),0,'/');
				//同步登录
				$ucsynlogin = uc_user_synlogin($uid);
				echo $ucsynlogin;
			}


			$this->success('登录成功',U('Member/User/index'));
		}
		else
		{

			//判断是否已经登录
			$this->_check_ucenter();

			$cms = $this->base();
			$this->assign('cms',$cms);
			$this->display();
		}
	}


	//退出
	public function out()
	{


		session('uid',null);
		session('username',null);
		session('email',null);

		//ucenter退出
		if(C('cfg_ucenter'))
		{
			include_once './Data/Config/uc.inc.php';
			include_once './uc_client/client.php';
			setcookie('tpcms_auth', '',-1,'/');
			$ucsynlogout = uc_user_synlogout();
			echo $ucsynlogout;
		}




		$this->success('退出成功',U('Member/Login/index'));
		//$this->redirect('Member/Login/index');
	}

}